
ar = new function() {

  var self = this;

  /**
   * Handeling name spaces.
   * @param {string} nameSpace
   * @param {any} reference
   * @return {object} ar
   */
  this.assign = function(nameSpace, reference) {

    var nameSpace = nameSpace.split('.'), name;

    (function recursion(currentNameSpace) {

      name = nameSpace.shift()

      if (nameSpace.length) {

	      if (!currentNameSpace[name])
	      	currentNameSpace[name] = {};

	      recursion(currentNameSpace[name])

      } else
        currentNameSpace[name] = reference;

    }(self));

    return this;

  }
}



ar.assign('load.dependencies', [])
ar.assign('load.evaluationMethods', [])
ar.assign('load.method', function(type, evaluationMethod) {
  ar.load.evaluationMethods[type] = evaluationMethod;
  ar.load[type] = function(path) {
    ar.load.archive.request(path, function() {

      if (!ar.load.archive.scope[path]) {
        ar.load.archive.satisfied[path] = [];
        ar.load.evaluate(path, type);
      }

    });
    return ar.load;
  }
})
ar.assign('load.archive.source', {})
ar.assign('load.archive.scope', {})
ar.assign('load.archive.request', function(path, callback) {

  ar.load.dependencies.push(path)

  if (!ar.load.archive.source[path])
    ar.load.download(path, callback);
  else callback();

})

ar.assign('load.archive.satisfied', {})
ar.assign('load.satisfied', function(path) {



})

ar.assign('load.whenSatisfied', function(path, callback) {

  if (ar.load.archive.scope[path])
    callback();
  else ar.load.archive.satisfied[path].push(callback);

})

ar.assign('load.evaluate', function(path, type) {

  console.log(type)

  var scope = new Function();

  ar.load.evaluationMethods[type].call(scope, {
    source: ar.load.archive.source[path],
    scope: ar.load.archive.scope[path],
    path: path,
  })

  ar.load.archive.scope[path] = scope;

})
ar.assign('load.download', function(path, callback) {

  var request = new XMLHttpRequest();
  request.open('GET', path, true);
  request.send();
  request.onload = function(event) {

    ar.load.archive.source[path] = this.responseText;
    callback()

  };

})
ar.assign('load.run', function() {

  var dependencies = ar.load.dependencies;

  dependencies.forEach(function(dependency) {

    ar.load.whenSatisfied(dependency, function() {

      console.log(dependency)

    })

  });

  ar.assign('load.dependencies', [])

});

ar.load.method('js', function(asset) {

  eval(asset.source);

});

ar.load.method('javascript', function(asset) {

  eval(asset.source);

});

ar.load.method('html', function(asset) {

  asset.scope[asset.path] = asset.source;

})

ar.load.method('css', function(asset) {

  var style = document.createElement('style');
    style.innerHTML = asset.source;

  document.head.appendChild(style);

})

            ar.load
              .js('src/subscribables/Attribute.js')
              .js('src/subscribables/Array.js')
              .js('src/subscribables/Function.js')
            .run(new Function())
