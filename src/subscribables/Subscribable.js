
function Subscribable() {

    this.updateValue = function(value) {

        /**
         * Update value.
         */
        this.value = value;

        /**
         * Push value to list of
         * notifications to be
         * broadcasted to subscribers.
         */
        this.notificationQueue.push(value);

        /**
         * Notify all subscribers if
         * notifications are not locked.
         */
        if (!this.locked)
            this.notifySubscribers();

        return this.value;

    }

    this.notifySubscribers = function() {

        var value;

        /**
         * Notify all subscribers to
         * changed values.
         */
        while (value = this.notificationQueue.shift()) {

            this.subscribers.forEach(function(callback) {

                callback(value);

            });
        }
    }

    this.lock = function() {

        this.locked = true;

    }

    this.unLock = function() {

        this.locked = false;

        /**
         * Notify all subscribers
         * that have queued up
         * sinse notifications
         * where locked.
         */
        this.notifySubscribers();

    }

    this.subscribe = function(callback) {

        /**
         * Add callback to
         * list of subscribers
         */
        this.subscribers.push(callback);

        return this;
    }
}

ar.assign('modules.Subscribable', Subscribable);

