
ar.load
  .js('src/subscribables/Subscribable.js')
  .run(function() {

    function SubscribableArray(input) {

      /**
       * Make sure different
       * instances does not
       * share any of these
       * paramenters
       */
      this.value = input;
      this.locked = false;
      this.notificationQueue = new Array();
      this.subscribers = new Array();


      this.pop = function() {

        this.value.pop.apply(this.value, arguments);
        return this.updateValue(this.value);

      }

      this.push = function() {

        this.value.push.apply(this.value, arguments);
        return this.updateValue(this.value);

      }

      this.reverse = function() {

        this.value.reverse.apply(this.value, arguments);
        return this.updateValue(this.value);

      }

      this.shift = function() {

        this.value.shift.apply(this.value, arguments);
        return this.updateValue(this.value);

      }

      this.sort = function() {

        this.value.sort.apply(this.value, arguments);
        return this.updateValue(this.value);
      }

      this.splice = function() {

        this.value.splice.apply(this.value, arguments);
        return this.updateValue(this.value);

      }

      this.unshift = function() {

        this.value.unshift.apply(this.value, arguments);
        return this.updateValue(this.value);

      }
    }
    SubscribableArray.prototype = new ar.modules.Subscribable();

    this.array = function(input) {

      /**
       * Make sure that input is an
       * array.
       */
      if (input instanceof Array === false)
        throw new ArException(input.toString() + ' is not an array.');

      /**
       * Create subscribable
       * wrapper.
       */
      var wrapper = function(input) {

        /**
         * If no new value return
         * the previous value.
         */
        if (typeof input === 'undefined')
          return wrapper.value;

        return wrapper.updateValue(input);
      }

      /**
       * Return wrapper extending
       * paramenters and methods from
       * the 'SubscribableArray' object.
       */
      return extend(wrapper, new SubscribableArray(input));
    }

});
