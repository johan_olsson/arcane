
ar.load
  .js('src/subscribables/Subscribable.js')
  .run(function() {

    function SubscribableFunction() {

      /**
       * Make sure different
       * instances does not
       * share any of these
       * paramenters
       */
      this.locked = false;
      this.notificationQueue = new Array();
      this.subscribers = new Array();

    }
    SubscribableFunction.prototype = new ar.modules.Subscribable();

    this.function = function(input) {

      /**
       * Make sure that input is a
       * function.
       */
      if (input instanceof Function === false)
        throw new ArException(input.toString() + ' is not a function.');

      /**
       * Stick input to scope.
       */
      var input = input;

      /**
       * Create subscribable
       * wrapper.
       */
      var wrapper = function() {
        var result = input.apply(this, arguments)
        return wrapper.updateValue(result);
      }

      /**
       * Return wrapper extending
       * paramenters and methods from
       * the 'SubscribableFunction' object.
       */
      return extend(wrapper, new SubscribableFunction(input));
    }

});
