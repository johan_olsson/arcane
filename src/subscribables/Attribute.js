
ar.load
  .js('src/subscribables/Subscribable.js')
  .run(function() {

    function SubscribableAttribute(input) {

      /**
       * Make sure different
       * instances does not
       * share any of these
       * paramenters
       */
      this.value = input;
      this.locked = false;
      this.notificationQueue = new Array();
      this.subscribers = new Array();


    }
    SubscribableAttribute.prototype = new ar.modules.Subscribable();

    this.attribute = function(input) {

      /**
       * Create subscribable
       * wrapper.
       */
      var wrapper = function(input) {

        /**
         * If no new value return
         * the previous value.
         */
        if (typeof input === 'undefined')
          return wrapper.value;

        /**
         * Return wrapper extending
         * paramenters and methods from
         * the 'SubscribableAttribute' object.
         */
        return wrapper.updateValue(input);
      }

      return extend(wrapper, new SubscribableAttribute(input));
    }

});
