
/**
 * Prototype extensions
 * for more browser
 * covagre.
 */
if (Array.hasOwnProperty('forEach')) {
    Array.prototype.forEach = function(callback) {
        var i=0,l=this.length;
        for (;i<l;i++) {
            callback(this[i], i);
        }
    }
}

/**
 * Utils and tools.
 */
var extend = function(target, source) {

    var key;
    for (key in source) {
        target[key] = source[key];
    }

    return target;
}

function ArException(message) {
    this.message = message;
    this.name = 'ArException';
}
