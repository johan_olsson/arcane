
load = new function() {

  /**
   * Modules.
   */
  function Events() {
    var self = this;

    /**
     * Create listeners object for
     * functions with shared listeners
     * (override this if they cannot be
     * shared)
     */
    this.listeners = {};

    this.dispatch = function(event, data) {

      this.listeners[event] = this.listeners[event] || [];

      this.listeners[event].forEach(function(listener) {
        listener(data);
      })

    }

    this.ignore = function(event, callback) {

      this.listeners[event].splice(this.listeners[event].indexOf(callback), 1);

    }

    /**
     * Modules.
     */
    this.listen = function(event, callback) {

      this.listeners[event] = this.listeners[event] || [];

      this.listeners[event].push(callback);

      return {
        ignore: function() {
          self.ignore(event, callback)
        }
      }
    }
  }


  function Archive() {

    this.listeners = {};

    var self = this;

    self.assets = {};

    self.fetchAsset = function(path, type) {

      if (typeof self.assets[path] === 'undefined') {
        self.assets[path] = new Asset(path);
        self.assets[path].fetch();
        self.assets[path].type = type;

      }

      self.dispatch('assetRequested', self.assets[path]);

    };
    self.evaluateAssets = function(list) {

      list.forEach(function(item) {
        if (!item.scope)
          item.evaluate();
      })
    }
  }
  Archive.prototype = new Events()


  function Asset(path) {

    this.listeners = {};

    var self = this;

    self.path = path;

    self.fetch = function() {

      if (preLoadedAssets[self.path]) {
        self.source = preLoadedAssets[self.path];
        self.dispatch('fetched', self);
        return;
      }

      var request = new XMLHttpRequest();
        request.open('GET', self.path, true);
        request.send();
        request.onload = function(event) {
          self.source = this.responseText;
          self.dispatch('fetched', self);
        };

    };
    self.evaluate = function() {

      self.scope = new Scope(self);

      var evaluate = function() {

        var listener = dependencies.listen('callbackCreated', function(callback) {
          self.callback = callback
          self.callback.listen('satisfied', function() {
            self.dispatch('satisfied')
          })
        })

        evaluationMethods[self.type].call(self.scope, self)

        listener.ignore();
        if (!self.callback)
          self.dispatch('satisfied')

      }

      if (self.source) {
        return evaluate()
      }
      self.listen('fetched', evaluate)
    };

  }
  Asset.prototype = new Events();

  function Scope() {}

  function Dependencies() {}
  Dependencies.prototype = new Events();

  function Callback(callback, dependencies) {
    this.dependencies = dependencies;
    this.listeners = {};
    this.callback = callback
  }
  Callback.prototype = new Events();

  var createScope = function(dependencies) {
    var k,i=0,l=dependencies.length;
    var scope = new Scope();

    for (;i<l;i++) {
      for (k in dependencies[i].scope) {
        scope[k] = dependencies[i].scope[k];
      }
    }
    return scope;
  }

  /**
   * Setup.
   */
  var dependencyTracker = new Array()
  var dependencies = new Dependencies();
  var evaluationMethods = {};

  this.archive = new Archive();

  this.archive.listen('assetRequested', function(asset) {
    dependencyTracker.push(asset);
  });


  /**
   * Public methods.
   */
  this.method = function(type, evaluationMethod) {
    evaluationMethods[type] = evaluationMethod;
    this[type] = function(path) {
      this.archive.fetchAsset(path, type);
      return load;
    }
  }

  this.run = function(callback) {

    var callback = new Callback(callback, dependencyTracker);
    var depend = dependencyTracker.length;

    dependencyTracker.forEach(function(dependency) {
      dependency.listen('satisfied', function() {
        if (dependencyTracker.every(function(value) { return !!value.scope }))
          if (callback.callback)
            callback.callback.call(createScope(callback.dependencies));
          callback.dispatch('satisfied');
      })
    })

    dependencies.dispatch('callbackCreated', callback)
    this.archive.evaluateAssets(dependencyTracker)

    dependencyTracker = new Array();
    return this;
  };
}

load.method('js', function(asset) {

  eval(asset.source);

});

load.method('javascript', function(asset) {

  eval(asset.source);

});

load.method('html', function(asset) {

  asset.scope[asset.path] = asset.source;

})

load.method('css', function(asset) {

  var style = document.createElement('style');
    style.innerHTML = asset.source;

  document.head.appendChild(style);

})





